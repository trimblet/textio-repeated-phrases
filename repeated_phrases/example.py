import ngrams

doc = "The Burr–Hamilton duel is one of the most famous personal conflicts in American history. It was a draw duel which arose from long-standing personal bitterness that developed between the two men over the course of several years. Tensions reached a boiling point with Hamilton's journalistic defamation of Burr's character during the 1804 New York gubernatorial race, in which Burr was a candidate. The duel was fought at a time when the practice was being outlawed in the northern United States, and it had immense political ramifications. Burr survived the duel and was indicted for murder in both New York and New Jersey, though these charges were later either dismissed or resulted in acquittal. The harsh criticism and animosity directed toward him following the duel brought an end to his political career. The Federalist Party, already weakened by the defeat of John Adams in the presidential election of 1800, was further weakened by Hamilton's death."

print(ngrams.top_phrases(doc, k=3))
print(ngrams.top_phrases(doc))
