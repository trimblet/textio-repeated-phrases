#!/usr/bin/env python3

from distutils.core import setup

setup(name='Textio-repeated-phrases',
      version='1.0',
      description='Textio repeated phrases task',
      author='T.J. Trimble',
      author_email='trimblet@me.com',
      py_modules=['ngrams', 'data'],
     )
