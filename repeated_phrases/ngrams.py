#####################
# Repeated phrases
#
# Module for retrieving the top ten repeated phrases
#
# Given a documentument (a string), return the top-10 most-frequently occurring phrases,
# from most frequent to least.  A phrase is a list of consecutive words,
# a phrase must be at least 2 and no more than 10 words long.
# Only return phrases that occur at least twice.  A phrase can’t span sentences.
#
#
# Assumptions:
#  * lowercase all words
#  * ignore punctuation (except sentence splitting)
#  * split function words into tokens (it's -> it 's)
#  * phrases overlap ("a a a" -> (("a", "a"), 2), (("a", "a", "a"), 1))
#  * sort by value then by length of sequence, returning longer sequences first
#
#####################

import string
import re
import operator

from collections import Counter

import data


def top_phrases(document, k=10, min=2):
    """
    Return the k top phrases from a given string of English text.
    """
    sentences = preprocess(document)
    phrases = get_phrases(sentences)

    phrases = {k: v for k, v in phrases.items() if v >= min}
    # Sort by value THEN by length of sequence, returning longer sequences first
    phrases = sorted(phrases.items(), key=lambda e: (e[1], len(e[0])), reverse=True)
    return phrases[:k]


def get_phrases(sentences, lo=2, hi=10):
    """
    Given a list of tokenized sentences, count the phrases from length l to length h
    """
    result = Counter()
    for sentence in sentences:
        # Brute force
        length = len(sentence)
        for n in range(lo, min(hi+1, length+1)):
            for i in range(0, length-n+1):
                result[tuple(sentence[i:i+n])] += 1
        # An alternative implementation could use a trie with multiple
        # pointers to keep track of the position of the current token
        # in each ngram it is part of. The trie would act as a map from
        # tokens to counts. After counting, the trie could be converted
        # into a dict for sorting

        # The brute force method
    return dict(result)


def preprocess(document):
    """
    Given a documentument of text, normalize case, tokenize, split sentences, remove punctuation
    """
    document = document.lower()
    tokens = tokenize(document)
    sentences = split_sentences(tokens)
    sentences = [list(filter(None, [remove_punctuation(t) for t in s if t not in data.eos_chars])) for s in sentences]
    return sentences


tokenizer = re.compile("|".join([
       r'(\w+\-\w+', # hyphenated words
       r'\'\w+', # words with apostrophes ('s, 't)
       r'|'.join(map(lambda x: x + "\.", data.abbreviations)), # list of abbreviations
       r'\w+', # words
       r'[{}])'.format(string.punctuation) # punctuation
     ]))


def tokenize(document):
    """
    Given a documentument of text, return a list of the tokens in the documentument.
    An empty documentument returns an empty list
    """
    return tokenizer.findall(document)


def split_sentences(tokens):
    """
    Given a list of tokens, return a list of lists of tokens representing sentence boundaries
    An empty list returns an empty list
    """
    sentences = []
    current = []
    for t in tokens:
        current.append(t)
        if t in data.eos_chars:
            sentences.append(current)
            current = []
    if current: sentences.append(current) # Get last with no punctuation
    return sentences


punctuation = set(string.punctuation)

def remove_punctuation(s):
    """
    Given a string, remove all punctuation characters from it
    """
    return ''.join(c for c in s if c not in punctuation)
