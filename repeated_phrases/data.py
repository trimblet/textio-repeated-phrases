abbreviations = {"mr", "mrs", "ms", "mx", "miss", "dr", "prof", "sen", "rep", "atty", "lt", "col", "gen", "sec", "messrs", "gov", "adm", "rev", "maj", "sgt", "cpl", "pvt", "capt", "st", "ste", "ave", "pres", "lieut", "rt", "hon", "brig", "cmdr", "comdr", "pfc", "spc", "supt", "det", "mt", "ft", "adj", "adv", "asst", "assoc", "ens", "insp", "mlle", "mme", "msgr", "sfc", "vs", "v", "alex", "wm", "jos", "cie", "a.k.a", "cf", "treas", "ph", "ph.D"}

eos_chars = {".", "?", "!"}
