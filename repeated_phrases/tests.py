import unittest
import ngrams

class TokenizationTests(unittest.TestCase):


    def test_tokenize_basic(self):
        data = "The horse raced passed the barn fell."
        expected = ['the', 'horse', 'raced', 'passed', 'the', 'barn', 'fell', '.']
        self.assertEqual(ngrams.tokenize(data.lower()), expected)


    def test_tokenize_multiple_sentences(self):
        data = "The horse raced passed the barn fell. Then it got up and kept going!"
        expected = ['the', 'horse', 'raced', 'passed', 'the', 'barn', 'fell', '.', 'then', 'it', 'got', 'up', 'and', 'kept', 'going', '!']
        self.assertEqual(ngrams.tokenize(data.lower()), expected)


    def test_tokenize_abbreviations(self):
        data = "Vice President Mr. Burr shoots Gen. Hamilton, Dr. Hosack says."
        expected = ["vice", "president", "mr.", "burr", "shoots", "gen.", "hamilton", ",", "dr.", "hosack", "says", "."]
        self.assertEqual(ngrams.tokenize(data.lower()), expected)


    def test_tokenize_clitics(self):
        data = "This isn't it's best feature, but it didn't hurt."
        expected = ["this", "isn", "'t", "it", "'s", "best", "feature", ",", "but", "it", "didn", "'t", "hurt", "."]
        self.assertEqual(ngrams.tokenize(data.lower()), expected)


    def test_tokenize_empty(self):
        data = ""
        expected = []
        self.assertEqual(ngrams.tokenize(data.lower()), expected)



class SentenceSplittingTests(unittest.TestCase):


    def test_sentence_splitter_one_sentence(self):
        data = ['the', 'horse', 'raced', 'passed', 'the', 'barn', 'fell', '.']
        expected = [['the', 'horse', 'raced', 'passed', 'the', 'barn', 'fell', '.']]
        self.assertEqual(ngrams.split_sentences(data), expected)


    def test_sentence_splitter_abbreviations_one_sentence(self):
        data = ["vice", "president", "mr.", "burr", "shoots", "gen.", "hamilton", ",", "dr.", "hosack", "says", "."]
        expected = [["vice", "president", "mr.", "burr", "shoots", "gen.", "hamilton", ",", "dr.", "hosack", "says", "."]]
        self.assertEqual(ngrams.split_sentences(data), expected)


    def test_sentence_splitter_abbreviations(self):
        data = ["vice", "president", "mr.", "burr", "shoots", "gen.", "hamilton", ",", "dr.", "hosack", "says", ".", "sec.", "hamilton", "perishes", "after", "a", "day", "of", "agony", "."]
        expected = [["vice", "president", "mr.", "burr", "shoots", "gen.", "hamilton", ",", "dr.", "hosack", "says", "."], ["sec.", "hamilton", "perishes", "after", "a", "day", "of", "agony", "."]]
        self.assertEqual(ngrams.split_sentences(data), expected)


    def test_sentence_splitter_full(self):
        data = ['the', 'burr', 'hamilton', 'duel', 'was', 'fought', 'between', 'prominent', 'american', 'politicians', 'aaron', 'burr', ',', 'the', 'sitting', 'vice', 'president', 'of', 'the', 'united', 'states', ',', 'and', 'alexander', 'hamilton', ',', 'the', 'former', 'secretary', 'of', 'the', 'treasury', ',', 'at', 'weehawken', ',', 'new', 'jersey', 'on', 'july', '11', ',', '1804', '.', 'the', 'duel', 'was', 'the', 'culmination', 'of', 'a', 'long', 'and', 'bitter', 'rivalry', 'between', 'the', 'two', 'men', '.', 'burr', 'shot', 'and', 'mortally', 'wounded', 'hamilton', ',', 'who', 'was', 'carried', 'to', 'the', 'home', 'of', 'william', 'bayard', ',', 'where', 'he', 'died', 'the', 'next', 'day', '.']
        expected = [['the', 'burr', 'hamilton', 'duel', 'was', 'fought', 'between', 'prominent', 'american', 'politicians', 'aaron', 'burr', ',', 'the', 'sitting', 'vice', 'president', 'of', 'the', 'united', 'states', ',', 'and', 'alexander', 'hamilton', ',', 'the', 'former', 'secretary', 'of', 'the', 'treasury', ',', 'at', 'weehawken', ',', 'new', 'jersey', 'on', 'july', '11', ',', '1804', '.'], ['the', 'duel', 'was', 'the', 'culmination', 'of', 'a', 'long', 'and', 'bitter', 'rivalry', 'between', 'the', 'two', 'men', '.'], ['burr', 'shot', 'and', 'mortally', 'wounded', 'hamilton', ',', 'who', 'was', 'carried', 'to', 'the', 'home', 'of', 'william', 'bayard', ',', 'where', 'he', 'died', 'the', 'next', 'day', '.']]
        self.assertEqual(ngrams.split_sentences(data), expected)


    def test_sentence_splitter_empty(self):
        data = []
        expected = []
        self.assertEqual(ngrams.split_sentences(data), expected)


    def test_sentence_splitter_no_punctuation(self):
        data = ["a", "a", "a"]
        expected = [["a", "a", "a"]]
        self.assertEqual(ngrams.split_sentences(data), expected)



class PreprocessTests(unittest.TestCase):

    def test_preprocess_simple(self):
        data = "A a, a."
        expected = [["a", "a", "a"]]
        self.assertEqual(ngrams.preprocess(data), expected)


    def test_preprocess_basic(self):
        data = "It's a test! Its purpose is unclear. Gen. Sec. Hamilton was killed by Burr."
        expected = [["it", "s", "a", "test"], ["its", "purpose", "is", "unclear"], ["gen", "sec", "hamilton", "was", "killed", "by", "burr"]]
        self.assertEqual(ngrams.preprocess(data), expected)


    def test_preprocess_commas(self):
        data = "so, so, so, so this is what it feels like to match wits."
        expected = [["so", "so", "so", "so", "this", "is", "what", "it", "feels", "like", "to", "match", "wits"]]
        self.assertEqual(ngrams.preprocess(data), expected)


    def test_preprocess_empty(self):
        data = ""
        expected = []
        self.assertEqual(ngrams.preprocess(data), expected)



class GetPhrasesTests(unittest.TestCase):

    def test_get_phrases_simple(self):
        data = [['a', 'a'], ['a', 'a']]
        expected = {('a', 'a'): 2}
        self.assertEqual(ngrams.get_phrases(data), expected)


    def test_get_phrases_basic(self):
        data = [['this', 'is', 'a', 'test']]
        expected = {('this', 'is'): 1,
                    ('is', 'a'): 1,
                    ('a', 'test'): 1,
                    ('this', 'is', 'a'): 1,
                    ('is', 'a', 'test'): 1,
                    ('this', 'is', 'a', 'test'): 1}
        self.assertEqual(ngrams.get_phrases(data), expected)


    def test_get_phrases_empty(self):
        data = ""
        expected = {}
        self.assertEqual(ngrams.get_phrases(data), expected)


    def test_get_phrases_long(self):
        data = "This is a test of the international broadcasting emergency network. I repeat, this is a test of the international broadcasting emergency network."
        expected = [(('this', 'is', 'a', 'test', 'of', 'the', 'international', 'broadcasting', 'emergency', 'network'), 2), (('this', 'is', 'a', 'test', 'of', 'the', 'international', 'broadcasting', 'emergency'), 2), (('is', 'a', 'test', 'of', 'the', 'international', 'broadcasting', 'emergency', 'network'), 2), (('this', 'is', 'a', 'test', 'of', 'the', 'international', 'broadcasting'), 2), (('is', 'a', 'test', 'of', 'the', 'international', 'broadcasting', 'emergency'), 2), (('a', 'test', 'of', 'the', 'international', 'broadcasting', 'emergency', 'network'), 2), (('this', 'is', 'a', 'test', 'of', 'the', 'international'), 2), (('is', 'a', 'test', 'of', 'the', 'international', 'broadcasting'), 2), (('a', 'test', 'of', 'the', 'international', 'broadcasting', 'emergency'), 2), (('test', 'of', 'the', 'international', 'broadcasting', 'emergency', 'network'), 2)]
        self.assertEqual(ngrams.top_phrases(data), expected)



class RepeatedPhrasesTest(unittest.TestCase):

    def test_top_phrases_simple(self):
        data = "a a. a a."
        expected = [(('a', 'a'), 2)]
        self.assertEqual(ngrams.top_phrases(data), expected)


    def test_top_phrases_short(self):
        data = "so, so, so, so this is what it feels like to match wits."
        expected = [(("so", "so"), 3), (("so", "so", "so"), 2)]
        self.assertEqual(ngrams.top_phrases(data), expected)


    def test_top_phrases_basic(self):
        data = "Sec. Hamilton was slain by Vice President Burr. Vice President Burr's legacy was defined by killing Sec. Hamilton."
        expected = [(("vice", "president", "burr"), 2), (("sec", "hamilton"), 2), (("vice", "president"), 2), (("president", "burr"), 2)]
        self.assertEqual(ngrams.top_phrases(data), expected)


    def test_top_phrases_too_short(self):
        data = "Hello World!"
        expected = []
        self.assertEqual(ngrams.top_phrases(data), expected)


    def test_top_phrases_none(self):
        data = "It's a test. Its purpose is unclear. Gen. Sec. Hamilton was killed by Aaron Burr."
        expected = []
        self.assertEqual(ngrams.top_phrases(data), expected)


    def test_top_phrases_long(self):
        data = "This is a test of the international broadcasting emergency network. I repeat, this is a test of the international broadcasting emergency network."
        expected = [(('this', 'is', 'a', 'test', 'of', 'the', 'international', 'broadcasting', 'emergency', 'network'), 2), (('this', 'is', 'a', 'test', 'of', 'the', 'international', 'broadcasting', 'emergency'), 2), (('is', 'a', 'test', 'of', 'the', 'international', 'broadcasting', 'emergency', 'network'), 2), (('this', 'is', 'a', 'test', 'of', 'the', 'international', 'broadcasting'), 2), (('is', 'a', 'test', 'of', 'the', 'international', 'broadcasting', 'emergency'), 2), (('a', 'test', 'of', 'the', 'international', 'broadcasting', 'emergency', 'network'), 2), (('this', 'is', 'a', 'test', 'of', 'the', 'international'), 2), (('is', 'a', 'test', 'of', 'the', 'international', 'broadcasting'), 2), (('a', 'test', 'of', 'the', 'international', 'broadcasting', 'emergency'), 2), (('test', 'of', 'the', 'international', 'broadcasting', 'emergency', 'network'), 2)]
        self.assertEqual(ngrams.top_phrases(data), expected)


    def test_top_phrases_no_repeats(self):
        data = "The Burr–Hamilton duel was fought between prominent American politicians Aaron Burr, the sitting Vice President of the United States, and Alexander Hamilton, the former Treasury Secretary, at Weehawken, New Jersey on July 11, 1804. Burr shot and mortally wounded Hamilton, who was carried to the home of William Bayard, where he died the next day."
        expected = []
        self.assertEqual(ngrams.top_phrases(data), expected)


    def test_top_phrases_sentence_break(self):
        data = "This is a test. This is. a test."
        expected = [(("this", "is"), 2), (("a", "test"), 2)]
        self.assertEqual(ngrams.top_phrases(data), expected)


    def test_top_phrases_lower_casing(self):
        data = "a a. A A."
        expected = [(("a", "a"), 2)]
        self.assertEqual(ngrams.top_phrases(data), expected)


    def test_top_phrases_ignore_punctuation(self):
        data = "a a, a"
        expected = [(("a", "a"), 2)]
        self.assertEqual(ngrams.top_phrases(data), expected)


if __name__ == '__main__':
    unittest.main()
